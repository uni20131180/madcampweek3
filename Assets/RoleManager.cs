﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class RoleManager : NetworkBehaviour
{
    public int role = 0;
    public int firstConnect = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public int GetRole()
    {
        return role++;
    }
    public int GetConnect()
    {
        return firstConnect++;
    }
}
