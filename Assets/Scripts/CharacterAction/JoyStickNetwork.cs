﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Networking;

public class JoyStickNetwork : NetworkBehaviour
{

    public Sprite[] SpriteImage;

    public Transform Player;
    public Transform Stick;
    public static Vector3 JoyVec;
    public static bool MoveFlag;

    public Image itemButton;

    private Vector3 StickFirstPos;
    private float Radius;
    public float speed;

    private int item = 0;

    void Start()
    {
        Radius = GetComponent<RectTransform>().sizeDelta.y * 0.5f;
        StickFirstPos = Stick.transform.position;

        float Can = transform.parent.GetComponent<RectTransform>().localScale.x;
        Radius *= Can;

        MoveFlag = false;
    }

    public int HaveItem()
    {
        return item;
    }
    //아이템을 먹었으면 그 해당하는 이미지를 달아줘야한다.
    public void SetItem(int i)
    {
        itemButton.overrideSprite = SpriteImage[i];
        this.item = i;
    }

    public void UseItem()
    {
        int usage_item = this.item;
        this.item = 0;
        itemButton.overrideSprite = SpriteImage[this.item];
        EffectItem(usage_item);
        //return usage_item;
    }

    public void EffectItem(int i)
    {
        switch (i)
        {
            case 0:
                break;
            case 1:
                if (Player.GetComponent<SoloCharacterScript>() != null)
                {
                    Player.GetComponent<SoloCharacterScript>().speed += 0.06f;
                    Invoke("ReturnSpeed", 10.0f);
                }
                else
                {
                    Player.GetComponent<MultiCharacterScript>().speed += 0.06f;
                    Invoke("ReturnSpeed", 10.0f);
                }
                break;
            case 2:
                if (Player.GetComponent<SoloCharacterScript>() != null)
                {
                    //Player.SetPositionAndRotation()
                    Invoke("ReturnSpeed", 10.0f);
                }
                else
                {
                    //Player.SetPositionAndRotation()
                    Invoke("ReturnSpeed", 10.0f);
                }
                break;
        }

    }

    void ReturnSpeed()
    {
        if (Player.GetComponent<SoloCharacterScript>() != null)
        {
            Player.GetComponent<SoloCharacterScript>().speed -= 0.06f;
        }
        else
        {
            Player.GetComponent<MultiCharacterScript>().speed -= 0.06f;
        }
    }

    void Update()
    {
        //Player.transform.Translate(Vector3.forward * Time.deltaTime * 10f * speed);
    }

    // 드래그
    public void Drag(BaseEventData _Data)
    {
        MoveFlag = true;
        PointerEventData Data = _Data as PointerEventData;
        Vector3 Pos = Data.position;
        JoyVec = (Pos - StickFirstPos).normalized;
        float Dis = Vector3.Distance(Pos, StickFirstPos);
        if (Dis < Radius)
            Stick.position = StickFirstPos + JoyVec * Dis;
        else
            Stick.position = StickFirstPos + JoyVec * Radius;
        //Player.eulerAngles = new Vector3(0, Mathf.Atan2(JoyVec.x, JoyVec.y) * Mathf.Rad2Deg, 0);
    }
    public void DragEnd()
    {
        Stick.position = StickFirstPos;
        JoyVec = Vector3.zero;
        MoveFlag = false;
    }


}