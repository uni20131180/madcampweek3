﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderUp : MonoBehaviour
{
    public float UpSpeed = 0.02f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKey(KeyCode.W))
        {
            other.transform.position += Vector3.up * UpSpeed;
            other.attachedRigidbody.useGravity = false;

            //var targetPosition = new Vector3(other.transform.position.x, transform.position.y, other.transform.position.z);
            //other.transform.LookAt(targetPosition);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            other.transform.position += Vector3.down * UpSpeed;
            other.attachedRigidbody.useGravity = false;
        }
        else if (Input.touchCount > 0)
        {
            other.transform.position += Vector3.up * UpSpeed;
            other.attachedRigidbody.useGravity = false;
        }
        else
        {
            other.transform.position += Vector3.down * UpSpeed;
            other.attachedRigidbody.useGravity = false;
        }


    }


    private void OnTriggerExit(Collider other)
    {
         other.attachedRigidbody.useGravity = true;
    }

}
