﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
 
public class JoyStick : MonoBehaviour {

    public Sprite[] SpriteImage;

    public Transform Player;     
    public Transform Stick;
    public static Vector3 JoyVec;
    public static bool MoveFlag;
    public static bool ItemClicked;
    public static int NetworkItem;

    public Image itemButton;

    private Vector3 StickFirstPos;     
    private float Radius;           
    public float speed;

    private int item = 0;

    void Start()
    {
        Radius = GetComponent<RectTransform>().sizeDelta.y * 0.5f;
        StickFirstPos = Stick.transform.position;

        float Can = transform.parent.GetComponent<RectTransform>().localScale.x;
        Radius *= Can;
 
        MoveFlag = false;
    }

    public int HaveItem()
    {
        return item;
    }
    //아이템을 먹었으면 그 해당하는 이미지를 달아줘야한다.
    public void SetItem()
    {
        var i = Random.Range(1, 3);
        itemButton.overrideSprite = SpriteImage[i];
        this.item = i;
        NetworkItem = i;
    }

    public void UseItem()
    {
        int usage_item = this.item;
        this.item = 0;
        itemButton.overrideSprite = SpriteImage[this.item];
        EffectItem(usage_item);
        //return usage_item;
    }

    public void EffectItem(int i)
    {
        switch (i)
        {
            case 0:
                break;
            case 1:
                if (Player.GetComponent<SoloCharacterScript>() != null)
                { 
                     Player.GetComponent<SoloCharacterScript>().speed += 0.06f;
                     Invoke("ReturnSpeed", 5.0f);
                }
                else
                {
                    Player.GetComponent<MultiCharacterScript>().speed += 0.06f;
                    Invoke("ReturnSpeed", 5.0f);
                }
                break;
            case 2:
                if (Player.GetComponent<SoloCharacterScript>() != null)
                {
                    Player.transform.position += new Vector3(0.0f, 7.0f, 0.0f);
                    //Player.SetPositionAndRotation()
                }
                else
                {
                    Player.transform.position += new Vector3(0.0f, 7.0f, 0.0f);
                    //Player.SetPositionAndRotation()
                }
                break;
        }
        if (i!=0)
        {
            ItemClicked = true;
        }

    }


    void ReturnSpeed()
    {
        if (Player.GetComponent<SoloCharacterScript>() != null)
        {
            Player.GetComponent<SoloCharacterScript>().speed -= 0.06f;
        }
        else
        {
            Player.GetComponent<MultiCharacterScript>().speed -= 0.06f;
        }
     }
 
    void Update()
    {
            //Player.transform.Translate(Vector3.forward * Time.deltaTime * 10f * speed);
    }
 
    // 드래그
    public void Drag(BaseEventData _Data)
    {
        MoveFlag = true;
        PointerEventData Data = _Data as PointerEventData;
        Vector3 Pos = Data.position;
        JoyVec = (Pos - StickFirstPos).normalized;
        float Dis = Vector3.Distance(Pos, StickFirstPos);
        if (Dis < Radius)
            Stick.position = StickFirstPos + JoyVec * Dis;
        else
            Stick.position = StickFirstPos + JoyVec * Radius;
        //Player.eulerAngles = new Vector3(0, Mathf.Atan2(JoyVec.x, JoyVec.y) * Mathf.Rad2Deg, 0);
    }
    public void DragEnd()
    {
        Stick.position = StickFirstPos;
        JoyVec = Vector3.zero;
        MoveFlag = false;
    }
 

}