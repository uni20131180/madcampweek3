﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MultiChoose : MonoBehaviour
{
    public void GoChaseWaiting()
    {
        SceneManager.LoadScene("MultiChaseWatingScene");
    }

    public void GoRSPWaiting()
    {
        SceneManager.LoadScene("MultiRSPGameWaiting");
    }

}
