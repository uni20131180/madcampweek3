﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SoloPlayDifficultChoose : MonoBehaviour
{
    public static int Difficult = 1;
    // Start is called before the first frame update
    public void GoSoloEasy()
    {
        Difficult = 1;
        SceneManager.LoadScene("SoloGamePlayScene");
    }
    public void GoSoloNormal()
    {
        Difficult = 2;
        SceneManager.LoadScene("SoloGamePlayScene");
    }
    public void GoSoloHard()
    {
        Difficult = 3;
        SceneManager.LoadScene("SoloGamePlayScene");
    }

}
