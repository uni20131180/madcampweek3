﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MazeGenerator : MonoBehaviour
{
    public GameObject Wall;
    public GameObject Ground;
    public GameObject Player;
    public GameObject Hole;
    public GameObject Star;
    public int GARO = 10;
    public int SERO = 10;
    public int MIN_FLOOR = 4;
    public int MAX_FLOOR = 8;
    public int MIN_LADDER = 5;
    public int MAX_LADDER = 8;
    private int MAZE_FLOOR = 0;
    public GameObject[] Items;

    public struct MazeRoom
    {
        public int up, down, left, right;
        public MazeRoom(int up, int down, int left, int right)
        {
            this.up = up;
            this.down = down;
            this.left = left;
            this.right = right;
        }

        public bool CheckAllRoom()
        {

            if ((this.up == 0) || (this.down == 0) || (this.left == 0) || (this.right == 0))
            {
                return false;
            }
            return true;
        }

    }

    public struct Coordinate
    {
        public int x, y;
        public Coordinate(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }

    Stack<Coordinate> coordinate = new Stack<Coordinate>();
    MazeRoom[,] mazeroom;

    readonly int UP = 1;
    readonly int DOWN = 2;
    readonly int LEFT = 4;
    readonly int RIGHT = 8;

    void MakeOneMaze(int floor)
    {
        mazeroom = new MazeRoom[GARO, SERO];
        // 방의 크기를 생성한다.
        for (int i = 0; i < GARO; i++)
        {
            for (int j = 0; j < SERO; j++)
            {
                mazeroom[i, j].up = 1;
                mazeroom[i, j].down = 1;
                mazeroom[i, j].left = 1;
                mazeroom[i, j].right = 1;
            }
        }
        // 시작점은 항상 0,0 으로 한다.
        coordinate.Push(new Coordinate(0, 0));
        while (coordinate.Count > 0)
        {
            ArrayList direction = new ArrayList();
            Coordinate c = coordinate.Pop();
            // 이 좌표 주변에 4개의 벽이 모두 부숴지지 않은 셀이 하나 이상 존재 한다면
            // 상 하 좌 우 순으로 체크한다.
            // 상은 c.y - 1 만 범위를 체크한다
            if (c.y > 0)
            {
                if (mazeroom[c.x, c.y - 1].CheckAllRoom())
                {
                    direction.Add(UP);
                }
            }
            // 하는 c.y + 1 만 범위를 체크한다
            if (c.y < SERO - 1)
            {
                if (mazeroom[c.x, c.y + 1].CheckAllRoom())
                {
                    direction.Add(DOWN);
                }
            }
            // 좌는 c.x - 1 만 범위를 체크한다
            if (c.x > 0)
            {
                if (mazeroom[c.x - 1, c.y].CheckAllRoom())
                {
                    direction.Add(LEFT);
                }
            }
            // 우는 c.x + 1 만 범위를 체크한다
            if (c.x < GARO - 1)
            {
                if (mazeroom[c.x + 1, c.y].CheckAllRoom())
                {
                    direction.Add(RIGHT);
                }
            }

            if (direction.Count == 0)
            {
                continue;
            }

            int Choice = Random.Range(0, direction.Count);
            Coordinate nextCoordinate = new Coordinate();
            if ((int)direction[Choice] == UP)
            {
                nextCoordinate.x = c.x;
                nextCoordinate.y = c.y - 1;
                mazeroom[c.x, c.y].up = 0;
                mazeroom[nextCoordinate.x, nextCoordinate.y].down = 0;

            }
            else if ((int)direction[Choice] == DOWN)
            {
                nextCoordinate.x = c.x;
                nextCoordinate.y = c.y + 1;
                mazeroom[c.x, c.y].down = 0;
                mazeroom[nextCoordinate.x, nextCoordinate.y].up = 0;
            }
            else if ((int)direction[Choice] == LEFT)
            {
                nextCoordinate.x = c.x - 1;
                nextCoordinate.y = c.y;
                mazeroom[c.x, c.y].left = 0;
                mazeroom[nextCoordinate.x, nextCoordinate.y].right = 0;
            }
            else if ((int)direction[Choice] == RIGHT)
            {
                nextCoordinate.x = c.x + 1;
                nextCoordinate.y = c.y;
                mazeroom[c.x, c.y].right = 0;
                mazeroom[nextCoordinate.x, nextCoordinate.y].left = 0;
            }

            coordinate.Push(c);
            coordinate.Push(nextCoordinate);
        }

        var mazeroomDebug = new int[GARO * 3, SERO * 3];
        for (int i = 0; i < GARO; i++)
        {
            for (int j = 0; j < SERO; j++)
            {
                mazeroomDebug[3 * i + 1, 3 * j + 1] = 1;
                if (mazeroom[i, j].up == 0)
                {
                    mazeroomDebug[3 * i + 1, 3 * j] = 1;
                }
                if (mazeroom[i, j].down == 0)
                {
                    mazeroomDebug[3 * i + 1, 3 * j + 2] = 1;
                }
                if (mazeroom[i, j].left == 0)
                {
                    mazeroomDebug[3 * i, 3 * j + 1] = 1;
                }
                if (mazeroom[i, j].right == 0)
                {
                    mazeroomDebug[3 * i + 2, 3 * j + 1] = 1;
                }
            }
        }

        //완성
        //2층부터 몇개의 랜덤 사다리를 설치할 것인가 결정한다
        //사다리는 5~7개가 존재한다.
        int ladderCount = 0;
        if (floor != 0)
        {
            ladderCount = Random.Range(MIN_LADDER, MAX_LADDER);
        }
        
        ArrayList ladderCoordinate = new ArrayList();

        for (int i = 0; i < ladderCount; i++)
        {
            Coordinate c;
            c.x = Random.Range(0, GARO);
            c.y = Random.Range(0, SERO);
            ladderCoordinate.Add(c);
        }

        for (int i = 0; i < GARO; i++)
        {
            for (int j = 0; j < SERO; j++)
            {
                Vector3 groundPosition = new Vector3(i * 8, 6 * floor, j * 8);
                var nohole = true;
                for(int count = 0; count < ladderCoordinate.Count; count++)
                {
                    if( ((Coordinate)ladderCoordinate[count]).x == i && ((Coordinate)ladderCoordinate[count]).y == j)
                    {
                        Instantiate(Hole, groundPosition, Quaternion.Euler(-90, 0, 0));
                        nohole = false;
                        ladderCoordinate.RemoveAt(count);
                        break;
                    }
                }
                if (nohole)
                {
                    Instantiate(Ground, groundPosition, Quaternion.Euler(0, 0, 0));
                }
                if (mazeroom[i, j].up == 1)
                {
                    Vector3 groundPosition2 = new Vector3(i * 8, 6 * floor + 3, j * 8 - 4);
                    Instantiate(Wall, groundPosition2, Quaternion.Euler(0, 0, 0));
                }
                if (mazeroom[i, j].down == 1)
                {
                    Vector3 groundPosition2 = new Vector3(i * 8, 6 * floor + 3, j * 8 + 4);
                    Instantiate(Wall, groundPosition2, Quaternion.Euler(0, 0, 0));
                }
                if (mazeroom[i, j].left == 1)
                {
                    Vector3 groundPosition2 = new Vector3(i * 8 - 4, 6 * floor + 3, j * 8);
                    Instantiate(Wall, groundPosition2, Quaternion.Euler(0, 90, 0));
                }
                if (mazeroom[i, j].right == 1)
                {
                    Vector3 groundPosition2 = new Vector3(i * 8 + 4, 6 * floor + 3, j * 8);
                    Instantiate(Wall, groundPosition2, Quaternion.Euler(0, 90, 0));
                }
            }
        }
        int garo = Random.Range(0, GARO);
        int sero = Random.Range(0, SERO);
        int nopi = Random.Range(0, MAZE_FLOOR);
        Instantiate(Star, new Vector3(garo*8, nopi*6, sero*8), Quaternion.Euler(0, 0, 0));

    }
    void ItemResume()
    {
        for (int k = 0; k < MAZE_FLOOR; k++)
        {
            int i = 0;
            while (i < 7 * SoloPlayDifficultChoose.Difficult)
            {
                int garo = Random.Range(0, GARO);
                int sero = Random.Range(0, SERO);
                Vector3 groundPosition = new Vector3(garo * 8, 6 * k + 1, sero * 8);
                Instantiate(Items[Random.Range(0, Items.Length)], groundPosition, Quaternion.Euler(0, 0, 0));
                i++;
            }
        }
        Invoke("ItemResume", 60.0f);
    }

    // Start is called before the first frame update
    void Start()
    {
        int Difficult = SoloPlayDifficultChoose.Difficult;
        if (Difficult == 1)
        {
            GARO = 10;
            SERO = 10;
            MIN_FLOOR = 3;
            MAX_FLOOR = 5;
        }
        else if (Difficult == 2)
        {
            GARO = 15;
            SERO = 15;
            MIN_FLOOR = 5;
            MAX_FLOOR = 7;
        }
        else
        {
            GARO = 20;
            SERO = 20;
            MIN_FLOOR = 6;
            MAX_FLOOR = 9;
        }
        int floor = Random.Range(MIN_FLOOR, MAX_FLOOR);
        MAZE_FLOOR = floor;
        for(int i = 0; i < floor; i++)
        {
            MakeOneMaze(i);
        }
        Player.transform.position = new Vector3(2f, 2f, 2f);
        ItemResume();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
