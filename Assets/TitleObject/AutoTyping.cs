﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoTyping : MonoBehaviour
{
    public float type_speed = 0.1f;
    public float wait_rate = 60.0f;
    private string originText = "";
    private Text textUI = null;
    private int textCount = 0;

    public float speed;
    private void Start()
    {
        textUI = gameObject.GetComponent<Text>();
        if(textUI != null)
        {
            originText = textUI.text;
        }
        if(originText.Length > 0)
        {
            textUI.text = "";
            StartCoroutine("Typing");
        }
    }
    // Update is called once per frame
    IEnumerator Typing()
    {
        
        yield return new WaitForSeconds(type_speed * Random.Range(2,4));
        if(originText.Length - textCount > 0)
        {
            textUI.text += originText[textCount++];
        }
        if(originText.Length == textCount)
        {
            textCount++;
        }
        if(originText.Length < textCount)
        {
            yield return new WaitForSeconds(type_speed * wait_rate);
            textCount = 0;
            textUI.text = "";
        }
        StartCoroutine("Typing");
    }
}
